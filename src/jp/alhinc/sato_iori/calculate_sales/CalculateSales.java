package jp.alhinc.sato_iori.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;



public class CalculateSales {
	public static void main(String[] args) throws IOException  {

		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		//支店コードと支店名の保持
		HashMap<String, String> branchNames = new HashMap<String, String>();
		HashMap<String, Long> branchMoneys = new HashMap<String,Long>();

		if(!fileInput(args[0], branchNames, branchMoneys, "branch.lst", "支店" , "^[0-9]{3}$")) {
			return;
		}

		// ディレクトリからファイルをすべて読み込む
		File dir = new File(args[0]);  // ディレクトリ指定
		File[] files = dir.listFiles(); // 指定ディレクトリ内のファイル読み込み
		List<String> list = new ArrayList<String>();

		for(int i = 0; i < files.length; i++) { // ファイルがある限り表示
			File amountFile = files[i];
			String fileName = amountFile.getName();

			// 読み込んだ中から,先頭から数字8桁且つ文末が｢rcd｣のファイルを検索
			if(amountFile.isFile() && fileName.matches("^[0-9]{8}.rcd$"))  { // 一致するファイルを検索
				list.add(fileName);
			}

		}

		int number = 0;
		for(int i = 0; i < list.size(); i++) { // ファイルがある限り表示
			String[] amountName = list.get(i).split("\\.");
			int splitNo =Integer.parseInt(amountName[0]);
			if(number != 0 || number != splitNo - 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
			number = splitNo;
		}

		long sum = 0;
		BufferedReader br = null;

		for(int i = 0; i < list.size(); i++) {
			try {
				String line;
				File amountFile = new File(args[0],list.get(i));
				String fileNames = amountFile.getName().toString();
				FileReader fr = new FileReader(amountFile);
				br = new BufferedReader(fr);

				List<String> amountList = new ArrayList<String>(); // 指定ファイルを格納する箱を作る
				while((line = br.readLine()) != null)  { // 一行読み込む
					amountList.add(line); // リストに一行ずつ格納
				}
				if(amountList.size() != 2) {
					System.out.println(fileNames + "のフォーマットが不正です");
					return;
				}
				if(!branchNames.containsKey(amountList.get(0))) {
					System.out.println(fileNames + "の支店コードが不正です");
					return;
				}

				if(!amountList.get(1).matches("^[0-9]+$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				long amountMoneys = Long.parseLong(amountList.get(1)); // 売り上げ金額をlongに変換
				sum = branchMoneys.get(amountList.get(0)) + amountMoneys; // 変換したものを合計して
				if(String.valueOf(sum).length() > 10) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				branchMoneys.put(amountList.get(0), sum ); // マップに反映

			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				try {
					if(br != null) {
						br.close();
					}
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}

		if(!fileOutput(args[0], branchNames, branchMoneys, "branch.out")) {
			return;
		}
	}
	public static boolean fileInput(String dirPath, HashMap<String, String> names, HashMap<String, Long> moneys, String fileName, String define, String cord) {

		BufferedReader br = null;

		try {
			File file = new File(dirPath, fileName); // 支店定義ファイル指定
			if(!file.exists()) {
				System.out.println(define + "定義ファイルが存在しません");
				return false;
			}

			FileReader fr = new FileReader(file); // ファイル読み込み
			br = new BufferedReader(fr); // バッファ


			String line;
			String[] splitLine = null;
			while((line = br.readLine()) != null) { // 一行ずつ読み込む,繰り返し
				splitLine = line.split(","); // カンマ区切りで分け、箱に入れる
				if(!splitLine[0].matches(cord) || splitLine.length != 2) {
					System.out.println(define + "定義ファイルのフォーマットが不正です");
					return false;
				}
				names.put(splitLine[0], splitLine[1]); // 支店コードと支店名に分ける
				moneys.put(splitLine[0], (long) 0);
			}

		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			try {
				if(br != null) {
					br.close();
				}
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}
		}
		return true;
	}

	public static boolean fileOutput(String dirPath, HashMap<String, String> names, HashMap<String, Long> moneys, String fileName) {

		PrintWriter pw = null;

		try {
			FileWriter fw = null;
			BufferedWriter bw = null;
			File outputFile = new File(dirPath,fileName);
			fw = new FileWriter(outputFile);
			bw = new BufferedWriter(fw);
			pw = new PrintWriter(bw);

			for(Iterator<String> iterator = names.keySet().iterator(); iterator.hasNext(); ) {
				String key = iterator.next();
				pw.println(key + "," + names.get(key) + "," + moneys.get(key) );
			}

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(pw != null) {
				pw.close();
			}

		}
		return true;


	}
}


